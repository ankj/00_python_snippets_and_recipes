#python
#matplotlib
#meshgrid
#array


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

n=256
x=np.linspace(-3,3,n)
y=np.linspace(-3,3,n)

# Meshgrid creates two grids of co-ordinates
# One grid creates a grid of every x for each value of y
# The other grid creates a grid of every y for eaxh value of x
# X,Y together then give all X,Y co-ordinates
# Using Meshgrid allows calculations based on array
# This is much faster than looping

X,Y=np.meshgrid(x,y)

Z=X*np.sinc(X**2+Y**2)

plt.pcolormesh(X,Y,Z,cmap=cm.gray)
plt.show()
