#python
#matplotlib
#scatter

"""
Marker using a tuple: (numsides, style, angle)
numsides: the number of sides
style: the style of the regular symbol:
    0: a regular polygon
    1: a star-like symbol
    2: an asterisk
    3: a circle (numsides and angle is ignored)
angle: the angle of rotation of the symbol
"""



import numpy as np
import matplotlib.pyplot as plt

a=np.random.standard_normal((100,2))
a+=np.array((-1,-1))
b=np.random.standard_normal((100,2))
b+=np.array((1,1))

plt.scatter(a[:,0],a[:,1],c='1.00',edgecolor='k',marker=(5,0,0),s=100)
plt.scatter(b[:,0],b[:,1],c='k',marker=(3,0,45),s=100)       

plt.show()
