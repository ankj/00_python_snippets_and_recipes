#python
#machine learning
#sklearn
#matplotlib
#pca
#principal component analysis
#eigen pairs


# Import data
import pandas as pd
df_wine=pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data',header=None)

# Split into training and test, and standardise
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import StandardScaler
X=df_wine.iloc[:,1:].values
y=df_wine.iloc[:,0].values
X_train,X_test,y_train,Y_test=train_test_split(X,y,test_size=0.3,random_state=0)
sc=StandardScaler()
X_train_std=sc.fit_transform(X_train)
X_test_std=sc.fit_transform(X_test)

# Calculate covariance matrix, Eigenvectors and Eigenvalues
import numpy as np
cov_mat=np.cov(X_train_std.T)
eigen_vals,eigen_vecs=np.linalg.eig(cov_mat)
# print('\nCovariance matrix:\n',cov_mat)
# print('\nEigen vectorsin columns:\n',eigen_vecs)
# print('\nEigen values:\n',eigen_vals)
# Note np.linalg.eigh may provide a more stable calculation than np.linalg.eig

# Variance explained is sum of Egen values used / total Eigen values
# Calculate cumulative variance explained
tot=sum(eigen_vals)
var_explained=[(i/tot) for i in sorted(eigen_vals,reverse=True)]
cum_var_explained=np.cumsum(var_explained)

# Plot cumulative variance explained
import matplotlib.pyplot as plt
plt.bar(range(1,14),var_explained,alpha=0.5,align='center',
        label='individual explained variance')
plt.step(range(1,14),cum_var_explained,where='mid',
         label='cumulative explained variance')
plt.ylabel('Explained variance')
plt.xlabel('Principal components')
plt.legend(loc='best')
plt.show()

# Note: PCA is unsupervised - it down not link data with class labels

# Prepare aigen_pairs to transform original data
eigen_pairs=[(np.abs(eigen_vals[i]),eigen_vecs[:,i]) for i in range(len(eigen_vals))]
eigen_pairs.sort(reverse=True)

number_to_select=2
w=np.asmatrix(eigen_pairs[0][1]).T
for i in range(1,number_to_select):
    x=np.asmatrix(eigen_pairs[i][1]).T
    w=np.hstack((w,x))

# One row can be transformed using the transforming matrix w:
# print(X_train_std[0].dot(w))
# Or the whole original matrix may be transformed into the new PCs:
X_train_pca=X_train_std.dot(w)

# Visualise the 2 PCAs:
colors =['r','b','g']
markers=['s','x','o']
for l,c,m in zip(np.unique(y_train),colors,markers):
    plt.scatter(X_train_pca[y_train==l,0],X_train_pca[y_train==l,1],c=c,
                label=l,marker=m)
plt.xlabel('PC 1')
plt.ylabel('PC 2')
plt.legend(loc='lower left')
plt.show()



