﻿#python
#stats
#seaborn

import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

index = np.arange(5)
y = index**2
up_error = y/2
down_error = y/4
error_bars = [down_error,up_error]

plt.errorbar(index,y,yerr=error_bars,fmt='-o',capsize=5,capthick=3)
plt.show()