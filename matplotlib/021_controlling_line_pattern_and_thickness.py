#python
#matplotlib
#line plot
#bar chart



# Line styles available:
#
#	* solid
#	* dashed
#	* dotted
#	* dashdot


# =========
# line plot
# =========


import numpy as np
import matplotlib.pyplot as plt

def pdf(x,mu,sigma): # estimated probability distribution for normal distribution
    a=1/(sigma*np.sqrt(2*np.pi))
    b=-1/(2*sigma**2)
    return_val=a*np.exp(b*(x-mu)**2)
    return(return_val)

x=np.linspace(-6,6,1024)

plt.plot(x,pdf(x,0,1),color='k',linestyle='solid')
plt.plot(x,pdf(x,0,.5),color='k',linestyle='dashed')
plt.plot(x,pdf(x,0,.25),color='k',linestyle='dashdot')

plt.show()


# =========
# bar chart
# =========

import numpy as np
import matplotlib.pyplot as plt

n=8
a=np.random.random(n)
b=np.random.random(n)
x=np.arange(n)

plt.bar(x,a,color='0.75',edgecolor='k')
plt.bar(x,a+b,bottom=a,color='w',linestyle='dashed',edgecolor='k')

plt.show()


# ==================
# multiple line plot
# ==================

import numpy as np
import matplotlib.pyplot as plt

def pdf(x,mu,sigma): # estimated probability distribution for normal distribution
    a=1/(sigma*np.sqrt(2*np.pi))
    b=-1/(2*sigma**2)
    return_val=a*np.exp(b*(x-mu)**2)
    return(return_val)

x=np.linspace(-6,6,1024)

for i in range(64):
    samples=np.random.standard_normal(50)
    mu,sigma=np.mean(samples),np.std(samples)
    plt.plot(x,pdf(x,mu,sigma),color='0.75',linewidth=0.5)

plt.plot(x,pdf(x,0,1),color='r',linewidth=3)

plt.show()







