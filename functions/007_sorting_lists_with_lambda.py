#python
#sort
#lambda
#dictionary

sort sorted itemgetter

Simple example of sorted:

x=[2,3,1]
y=sorted(x)
y
Out[6]: [1, 2, 3]

Sorting lists with lambda function

my_list=[[10,2],[10,3],[10,1],[2,10],[5,7],[0,1],[1,0]]
f=lambda x:x[0]
print(sorted(my_list,key=lambda x:x[0]))
# sort on two in reverse order
print(sorted(my_list,key=lambda x:[x[0],x[1]]))
print(sorted(my_list,key=lambda x:[x[0],x[1]],reverse=True))


Sorting dictionaries with itemgetter

# Sort a list of a dicts on a common key

rows = [
    {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003},
    {'fname': 'David', 'lname': 'Beazley', 'uid': 1002},
    {'fname': 'John', 'lname': 'Cleese', 'uid': 1001},
    {'fname': 'Big', 'lname': 'Jones', 'uid': 1004}
]

from operator import itemgetter

# Item getter returns values that sorted() can work with

rows_by_fname = sorted(rows, key=itemgetter('fname'))
rows_by_uid = sorted(rows, key=itemgetter('uid'))

from pprint import pprint

print("Sorted by fname:")
pprint(rows_by_fname)

print("Sorted by uid:")
pprint(rows_by_uid)

rows_by_lfname = sorted(rows, key=itemgetter('lname','fname'))
print("Sorted by lname,fname:")
pprint(rows_by_lfname)


This may also be achieved using a lambda function:

rows_by_fname = sorted(rows, key=lambda r: r['fname'],r['lname']
