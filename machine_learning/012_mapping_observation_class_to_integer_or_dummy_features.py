#python
#machine learning
#sklearn


import pandas as pd

# Jump to end for Dummies

# ordinal catagories may be sort, e.g. S,M,L,XL
# nominal catagoties jave no natural sort order, e.g. blue, red, green

df=pd.DataFrame([['green','M',10.1,'class1'],
                 ['red','L',13/5,'class2'],
                 ['blue','XL',15.3,'class1']])
df.columns=['color','size','price','classlabel']
print('\nDateFrame:\n',df)

# Map size to integer:
size_mapping={'XL':3,'L':2,'M':1}
df['size']=df['size'].map(size_mapping)
print('\nSize mapped to integer:\n',df)

# Reverse map
#inv_size_mapping={v: k for k,v in size_mapping.items()}
#df['size']=df['size'].map(inv_size_mapping)
#print('\nReverse map:\n',df)

# Though sckikit learn will usually automattically assign text labels to integers,
# it is considered good practice to do this before passing data to scikit learn
# We can do this automatically:

import numpy as np
# enumerate creates a list of numbered tuples for a list of values
class_mapping={label:idx for idx,label in enumerate(np.unique(df['classlabel']))}
print ('\nClass mapping:\n',class_mapping)
print()
# Use class_mapping to xonvert names to integers
df['classlabel']=df['classlabel'].map(class_mapping)
print(df)
print()

# map class integer back to class label
inv_class_mapping={v:k for k,v in class_mapping.items()}
df['classlabel']=df['classlabel'].map(inv_class_mapping)
print(df)
print()

# Or use scikit learn label encoder class
from sklearn.preprocessing import LabelEncoder
class_le=LabelEncoder()
y=class_le.fit_transform(df['classlabel'].values)
print(y) # this is an array for class label
print()
# Print the inversion
print(class_le.inverse_transform(y))
print()

# Converting colours to integers and then to dummy features
# Categorical values should not be left as integer which implies order
# Create object array of values from df
X=df[['color','size','price']].values
color_le=LabelEncoder()
X[:,0]=color_le.fit_transform(X[:,0])
print(X) # Color has been coded as integer
print()

# Use OneHotEncoding to create dummy values
from sklearn.preprocessing import OneHotEncoder
ohe = OneHotEncoder(categorical_features=[0])
print(ohe.fit_transform(X).toarray()) # toarray converts sparse matrix to numpy array for displaying
print()
# ohe = OneHotEncoder(categorical_features=[0],sparse=False) would return a regular Numpy array


# Or more simply use pd.get_dummies method
print(pd.get_dummies(df[['price','color','size']]))
print()
