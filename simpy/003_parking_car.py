#python
#simpy

import simpy

def main():
    env=simpy.Environment()
    env.process(car(env))
    env.run(until=40)


def car(env):
    while True:
        # Parking
        print('Start parking car at %d' % env.now)
        parking_duration = 5
        yield env.timeout(parking_duration)
        # Driving
        print('Start driving car at %d' % env.now)
        trip_duration=20
        yield env.timeout(trip_duration)
    
if __name__ == '__main__':
    main()
    
