﻿#seaborn
#python
#stats
#histogram
#kernal density plot
#kde


import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

# Generate data that are normally distributed
x = np.random.randn(500)

# Histogram
plt.hist(x,bins=25)
plt.show()

#Kernal density plot
sns.kdeplot(x,shade=True)
plt.show()