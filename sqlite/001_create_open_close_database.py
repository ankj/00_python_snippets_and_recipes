#python
#sqlite

import sqlite3

con=sqlite3.connect('my_database.db') # opens or creates a database
cur=con.cursor() # set up cursor to send commands to

con.commit() # commit changes
con.close() # close database
