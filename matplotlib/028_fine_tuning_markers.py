#python
#matplotlib
#line plot


import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-6,6,1024)
y=np.sinc(x)

plt.plot(x,y,
         linewidth=3,
         color='k',
         markersize=9,
         markeredgewidth=1.5,
         markerfacecolor='0.75',
         markeredgecolor='k',
         marker='o',
         markevery=32)

plt.show()
