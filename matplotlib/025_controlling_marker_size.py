#python
#matplotlib
#scatter


import numpy as np
import matplotlib.pyplot as plt

a=np.random.standard_normal((100,2))
a+=np.array((-1,-1))
b=np.random.standard_normal((100,2))
b+=np.array((1,1))

plt.scatter(a[:,0],a[:,1],c='1.00',edgecolor='k',marker='o',s=100)
plt.scatter(b[:,0],b[:,1],c='k',marker='o',s=25)       

plt.show()


# ===============================================
# Size may also be taken from a list or an array:
# ===============================================

import numpy as np
import matplotlib.pyplot as plt

m=np.random.standard_normal((100,2))
r=np.sum(m**2,axis=1)

plt.scatter(m[:,0],m[:,1],c='w',edgecolor='k',marker='s',s=32*r)

plt.show()
