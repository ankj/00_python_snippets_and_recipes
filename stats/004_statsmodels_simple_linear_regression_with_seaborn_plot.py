﻿#python
#stats
#seaborn


import numpy as np
import pandas as pd
import statsmodels.formula.api as sm
import seaborn as sns

# Generate a noisy line, and save to panda DF

x = np.arange(100)
y = 0.5 * x - 20 + np.random.randn(len(x))*10
df = pd.DataFrame({'x':x, 'y':y})

# fit a linear model
model = sm.ols('y~x', data=df).fit()
print(model.summary())

# plt with seaborn
sns.regplot('x','y',data=df)