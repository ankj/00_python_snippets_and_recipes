#python
#oop
#class
#global

class G():
    interest_rate=0.1

# Reference global attribute. No instance of object is required.
print(G.interest_rate)

# Add new global attribute
G.inflation_rate=0.05

# Reference new global attribute
print (G.inflation_rate)

#Note: attributes can also be added to objects independent of their class.
#In such cases the attribute is only available in the object instance where it was set.
T#rying to access the same attribute in another instance will cause an error
