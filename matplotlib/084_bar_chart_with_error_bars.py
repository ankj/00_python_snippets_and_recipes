#python
#matplotlib
#bar chart
#error bar

import matplotlib.pyplot as plt
import numpy as np

x=[1,2,3,4]
y=[5.,25.,50.,20.]
yerr_upper=[2,10,5,3]
yerr_lower=np.zeros(len(yerr_upper))

fig, ax = plt.subplots()
ax.bar(x,y,
        width=0.8,
        color='0.5',
        edgecolor='k',
        yerr=[yerr_lower,yerr_upper],
        linewidth = 2,
        capsize=10)

ax.set_xticks(x)
ax.set_xticklabels(('G1', 'G2', 'G3', 'G4'))

plt.show()


