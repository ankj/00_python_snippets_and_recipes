#python
#while
#else

The else clause is only executed when your while condition becomes false. 
If you break out of the loop, or if an exception is raised, it won't be executed.


while value < threshold:
    if not process_acceptable_value(value):
        # something went wrong, exit the loop; don't pass go, don't collect 200
        break
    value = update(value)
else: # runs only if while ened with a false (not with a break)
    # value >= threshold; pass go, collect 200
    handle_threshold_reached()