#python
#matplotlib
#subplot

import numpy as np
import matplotlib.pyplot as plt

t=np.linspace(-np.pi,np.pi,1024)

fig,(ax0,ax1)=plt.subplots(ncols=2) # or ncols
ax0.plot(np.sin(2*t),np.cos(0.5*t),c='k')
ax1.plot(np.cos(3*t),np.sin(t),c='k')

plt.show()
