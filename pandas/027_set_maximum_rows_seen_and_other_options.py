#python
#pandas
#format

from pandas import set_option
set_option("display.max_rows", 16)

Available options:

Available options:

display.[chop_threshold, colheader_justify, column_space, date_dayfirst, date_yearfirst, encoding, expand_frame_repr, float_format, height, large_repr]
display.latex.[escape, longtable, repr]
display.[line_width, max_categories, max_columns, max_colwidth, max_info_columns, max_info_rows, max_rows, max_seq_items, memory_usage, mpl_style, multi_sparse, notebook_repr_html, pprint_nest_depth, precision, show_dimensions]
display.unicode.[ambiguous_as_wide, east_asian_width]
display.[width]
html.[border]
io.excel.xls.[writer]
io.excel.xlsm.[writer]
io.excel.xlsx.[writer]
io.hdf.[default_format, dropna_table]
mode.[chained_assignment, sim_interactive, use_inf_as_null]

Documentation: http://pandas.pydata.org/pandas-docs/stable/generated/pandas.set_option.html


