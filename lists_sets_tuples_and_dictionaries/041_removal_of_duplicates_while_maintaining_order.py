#python
#duplicates
#unique

Removing duplicates from a sequence while also maintaining order
The following is for hashable sequences (e.g. lists)

def dedupe(items):
    seen=set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)

a=[1,5,2,1,9,1,5,10,1,2,11]
b=list(dedupe(a))
print(b)
#Output: [1, 5, 2, 9, 10, 11]

The same function may be used for other examples, such as removing duplicate lines in a file:

"""Input file (test.txt):
This is the first line
This is the second line
This is the first line
This is the third line
This is the second line
This is the final line
"""

with open('test.txt','r') as f:
    for line in dedupe(f):
        print (line,end="")

"""Output:
This is the first line
This is the second line
This is the third line
This is the final line
"""

