calculate standard error:

se = sqrt (p(1-p)/n)

confidence interval (95%):

ci = mean +/- se * tn(0.025)

or:

statsmodels.stats.proportion.proportion_confint(count, nobs, alpha=0.05, method='normal')

e.g.

import statsmodels.stats.proportion
In[7]: statsmodels.stats.proportion.proportion_confint(5,10, alpha=0.05, method='normal')
Out[7]: (0.19010248384771916, 0.80989751615228078)


In [8]: statsmodels.stats.proportion.proportion_confint(50,100, alpha=0.05, method='normal')
Out[8]: (0.4020018007729973, 0.5979981992270027)

