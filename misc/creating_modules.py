#python
#module
#oop

Creating modules

A module is a Python file saved in the same directory as the programme.

This module was written and saved as "Dogs"





class Dog(object):
    def bark(self):
        print("Baarrrrkkkkk")

def woof():
    print("Wooooffffff!!!")

This is referenced by the main programme, including an import at the start:

import Dogs
Dogs.woof()
Burtel=Dogs.Dog()
Burtel.bark()
