#python
#pygame

blit is 'bit lock transfer', and copies an image from one surface to another.

Call blit from the destination object (often the display) and reference the source surface,
followed by the co-ordinate you want to blit to, e.g.:

screen.blit(background,(0,0))

If an image has frames (e.g. a walking image may be made by having different stages of walking at
different points in a master image) then we can refer to the frame number...

screen.blit(ogre,(300,100),(100*frame_number,0,100,100))
