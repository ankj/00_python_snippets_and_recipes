mport numpy as np

# Numpy arrays

x= np.array([0,0,0,1], dtype = bool)
print(x)

# Converting to boolean
# Note any number other than 0 is cast as True
y = np.array([0,1,2,0,3])
y = np.array(y, dtype = bool)
print (y)


# Converting to integer
x=np.array([1.3,5.6,7.8])
x = np.array(x, dtype = int)
print(x)

# Converting Python lists to boolean

x = [0,1,2]
x = [bool(n) for n in x]
print(x)

