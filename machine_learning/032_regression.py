import pandas as pd
"""
    1. CRIM      per capita crime rate by town
    2. ZN        proportion of residential land zoned for lots over 
                 25,000 sq.ft.
    3. INDUS     proportion of non-retail business acres per town
    4. CHAS      Charles River dummy variable (= 1 if tract bounds 
                 river; 0 otherwise)
    5. NOX       nitric oxides concentration (parts per 10 million)
    6. RM        average number of rooms per dwelling
    7. AGE       proportion of owner-occupied units built prior to 1940
    8. DIS       weighted distances to five Boston employment centres
    9. RAD       index of accessibility to radial highways
    10. TAX      full-value property-tax rate per $10,000
    11. PTRATIO  pupil-teacher ratio by town
    12. B        1000(Bk - 0.63)^2 where Bk is the proportion of blacks 
                 by town
    13. LSTAT    % lower status of the population
    14. MEDV     Median value of owner-occupied homes in $1000's
    """

# Load data from and save as CSV file
def load_data_from_web_and_save_csv():
    """Load data from web and save to local PC. Use if data has not been loaded
    and saved previously"""
    
    filename = 'https://archive.ics.uci.edu/ml/machine-learning-databases' + \
        '/housing/housing.data'
    df = pd.read_csv(filename, header = None, sep='\s+')
    df.columns = ['CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', 'DIS'] + \
        ['RAD', 'TAX', 'PTRATIO', 'B', 'LSTAT', 'MEDV']
    df.to_csv('data/housing.csv', index = False)
    
#load_data_from_web_and_save_csv()
df = pd.read_csv('data/life_expectancy.csv')

#