#python
#dictionary
#set

a = {'x':1,'y':2,'z':3,'zz':2}
b = {'x':10,'w':11,'y':2,'yy':2}
# Show keys in common
print(a.keys() & b.keys())
# Show keys in a not in b
print(a.keys()-b.keys()) # this subtract method works with sets {} but not lists []
# Show keys+values in common (items() return key+value, so both need to be the same)
print(a.items() & b.items())

# iterate through dictionary pairs
print({key:a[key] for key in a.keys()})

# remove items
removal_set={'z','zz'}
new_dict = {key:a[key] for key in a.keys() - removal_set}
