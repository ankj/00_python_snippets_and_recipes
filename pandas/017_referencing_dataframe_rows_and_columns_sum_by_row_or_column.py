#python
#pandas

import numpy as np
import pandas as pd

df1=pd.DataFrame({'growth':[0.5,0.7,1.2],'Name':['Paul','George','Ringo']})

# Access row using iloc
print(df1.iloc[2])

# Access column using header
x=df1['Name'] # this extracts a column as a Series
print(x)

# When a single column is accessed all Series funcrions may be applied to it

# Create a dataframe from rows
df2=pd.DataFrame([
    {'growth':0.5,'Name':'Paul'},
    {'growth':0.7,'Name':'George'},
    {'growth':1.2,'Name':'Ringo'}
    ])

# Use pd.read_csv(filename) to import from csv

# Create dataframe from numpy array (column names need to be specified)
nparray=np.random.randn(10,3)
df3=pd.DataFrame(nparray) # columns have integer name
df3.columns=['a','b','c'] # add column names
# Or specify column names directly
df4=pd.DataFrame(nparray,columns=['a','b','c'])

# show axes
# axis 0 = row
# axis 1 = columns (index)
print(df1.axes)
print(df1.axes[0])
print(df1.axes[1])

# To sum a column we sum all rows, sum by index[0]
# We can apply a numpy function to sum
df3_sum=df3.apply(np.sum,axis=0)
print(df3_sum)

# To sum along a row we sum by axis=1
df3_sum_row=df3.apply(np.sum,axis=1)
print(df3_sum_row)

