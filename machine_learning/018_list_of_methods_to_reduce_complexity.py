#python
#machine learning



	* Regularisation (especially L1 via logistic regression)
	* Sequential feature selection
	* Random Forests importance (but highly correlated data may end with one as high importance and the other as low importance)
	* Dimensionality reduction (compressing data into fewer features)
	* 
		* Principal Component Analysis (PCA, for unsupervised learning)
		* Linear Discriminant Analysis (LDA, for supervised learning)
		* Kernal Principal Component Analysis for non-linear reduction


