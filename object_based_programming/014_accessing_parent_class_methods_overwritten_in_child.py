#python
#oop
#super
#class

Super creates an object as an instance of the parent class (overwrting child __init__ method) or, as below, allows access to a parent method that had been over-written by child method of the same name


class MyContacts:
    def __init__(self,name):
        self.name = name
    def print_name(self):
        print (self.name)
    def shout(self):
        print('Hello from parent')
        
class MyFamily(MyContacts):
    def __init__(self,name,phone):
        self.phone=phone
        super().__init__(name)
    def shout(self): # This overrides parent shout method
        print('Hello from my family')
    def super_shout(self):
        super().shout()
    
x = MyContacts('bob')
y = MyFamily('Megan',999)

y.print_name() # uses inherited method
y.shout() # uses child method that had over-written parent method
y.super_shout() # accesses over-written parent method

Output:
Megan
Hello from my family
Hello from parent
