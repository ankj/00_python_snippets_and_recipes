#python
#pygame

Events can be simulated, 
e.g. the following will add a space bar presss to the end of the event queue.

my_event = pygame.event.Event(KEYDOWN, key=K_SPACE, mod=0, unicode=u' ')
pygame.event.post(my_event)

Or use a dicitonary:
my_event = pygame.event.Event(KEYDOWN, {"key":K_SPACE, "mod":0, "unicode":u' '})
pygame.event.post(my_event)


User-defined events
===================

USEREVENT below is the maximum number that pygame uses for events

CATONKEYBOARD = USEREVENT+1
my_event = pygame.event.Event(CATONKEYBOARD, message='Bad Cat!')
pygame.event.post(my_event)

User-defined events are handled in the same way as normal events:
for event in pygame.event.get():
    if event.type == CATONKEYBOARD:
        print(event.message)
