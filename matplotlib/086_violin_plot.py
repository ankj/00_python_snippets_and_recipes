#python
#matplotlib
#violin plot

import numpy as np
import matplotlib.pyplot as plt

n_violins = 5
positions = range(n_violins)
samples = [np.random.normal(3*i,3,100) for i in range (n_violins)]

result = plt.violinplot (samples,
                         positions,
                         points=300, # the more the smoother
                         widths=0.8,
                         showmeans=False,
                         showextrema=True,
                         showmedians=True)

for v in result['bodies']:
    v.set_facecolor('b')
    v.set_edgecolor('k')
    v.set_linewidth(1)
    v.set_alpha(1)

plt.show()


