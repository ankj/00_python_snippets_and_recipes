#python
#pandas
#matplotlib
#bar chart
#line plot


import pandas as pd
import matplotlib.pyplot as plt 

songs_69 = pd.Series([18.0,22.0,7.0,5.0],
                     index=['John','Paul','George','Ringo'],
                     name='Counts_69')
                     
songs_66 = pd.Series([5.0,7.0,18.0,22.0,0],
                     index=['Ringo','George','John','Paul','Eric'],
                     name='Counts_66')


# Plot line chart
fig=plt.figure() # only required for complex formating or to save
songs_69.plot() # this could be used alone in iPython
songs_66.plot()
plt.legend()
fig.savefig('ex1.png')

#Plot mixed bar and line chart
fig=plt.figure() # only required for complex formating or to save
songs_69.plot(kind='bar') # this could be used alone in iPython
songs_66.plot()
plt.legend()
fig.savefig('ex2.png')

#Plot stacked
fig=plt.figure() # only required for complex formating or to save
songs_69.plot(kind='bar') # this could be used alone in iPython
songs_66.plot(kind='bar',color='k',alpha=0.5) # k=black, alpha=tranparency
plt.legend()
fig.savefig('ex3.png')
