# Define a class
# The first part is the 'constructor' with __init__
# It is called when a new object instance of a class is initiated
# All normal class constructors and methods start with 'self'. Don't worry why.

class cat:
    def __init__(self,colour,legs,name):
        # This is the default set of data that this class holds
        self.name=name
        self.legs=legs
        self.colour=colour
    
    # Define a method. The object knows its own name so can use that...
    def say_hello(self, repeats=5): # =5 is defaukt if no value is passed
        for i in range(repeats):
            print('Meowww, my name is',self.name)
        return # not strictly needed here as no value is returned

# Initiate an object (an instance of a class):        
felix=cat(colour='tabby',name='Felix',legs=3)

# Access some data from that object use the dot format
print (felix.colour)
print () # print an empty line

# Call an object method, again with the dot format
felix.say_hello(3)
print ()

# Now see what happens if no value is passed; it uses the method default value
felix.say_hello()

