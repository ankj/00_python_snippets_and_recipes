﻿#seaborn
#python
#stats
#violin plot

import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

# Generate data
nd = stats.norm
data = nd.rvs(size=1000)

nd2 = stats.norm(loc=3,scale=1.5)
data2 = nd2.rvs(size=1000)

# Create dataframe 
df = pd.DataFrame({'Girls':data, 'Boys':data2})
sns.violinplot(df)
plt.show()