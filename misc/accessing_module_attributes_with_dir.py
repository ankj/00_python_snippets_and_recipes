#python
#module
#attributes

Save the following as example_module.py

# Use module to store attributes
title="The meaning of life"
food="Spam"
snake="Python"
print('This is printed from example_module')

The following is run in another program (saved in the same folder as example_module.py) to access the attribute title from example_module.py

import example_module
x=example_module.title
print(x)
# Atrribute may be imported and stored as variable with same name
from example_module import title
print(title)
# Multiple attributes may be imported:
from example_module import food,snake
# All module attributes may be imported .....
from example_module import *
print(food)
print(snake)

When the calling module is run 'This is printed from example_module' will be printed - import runs the named module.

The function dir can be used to list attributes in a module:

dir(example_module)

Out[10]:
['__builtins__',
 '__cached__',
 '__doc__',
 '__file__',
 '__loader__',
 '__name__',
 '__package__',
 '__spec__',
 'food',
 'snake',
 'title']

Note: using 'module.attribute' instead of 'from module import attribute'  avoids the risk of conflict between two attributes/variables (avoids over-writing a variable) because attributes with the same name are kept separate when referenced via their module.
