#python
#matplotlib
#array
#mandlebrot

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def iter_count(c,max_iter):
    x=c
    for n in range(max_iter):
        if abs(x)>2:
            return (n)
        x=x**2+c
    return (max_iter)

n=512
max_iter=64
xmin,xmax,ymin,ymax=-2.2,0.8,-1.5,1.5

x_series=np.linspace(xmin,xmax,n)
y_series=np.linspace(ymin,ymax,n)
z=np.empty((n,n))

for i,y in enumerate(y_series):
    for j,x in enumerate(x_series):
        z[i,j]=iter_count(complex(x,y),max_iter)

plt.imshow(z,cmap=cm.gray) # imshow = image show
plt.show()

# Co-ordinates in image are the array indexes


# Alternative plot:

plt.imshow(z,cmap=cm.binary,extent=(xmin,xmax,ymin,ymax))
# extent converts data/axes to scaled by a tuple of values
plt.show()
