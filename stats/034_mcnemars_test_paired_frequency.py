"""
Example below shows presence of disease before and after treatment.
Two rows are whether diease is present (row 1) or absent (row 2) before 
treatment. The two columns are whether the disease is present (col 1) or absent
(col 2) after treatment
"""



from statsmodels.sandbox.stats.runs import mcnemar
import numpy as np

f_obs = np.array([[101, 121],[59, 33]])
(statistic, pVal) = mcnemar(f_obs)
print('\nMCNEMAR\'S TEST -----------------------------------------------------')
print('p = {0:5.3f}'.format(pVal))
if pVal < 0.05:
    print("There was a significant change in the disease by the treatment.") 
