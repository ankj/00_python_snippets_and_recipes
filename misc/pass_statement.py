#python

The pass statement does nothing. 
It can be used when a statement is required syntactically but the program requires no action. 
It may be used as a placeholder until further code is written.

for i in range(10):
   pass