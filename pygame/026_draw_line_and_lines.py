#python
#pygame

use aaline and aalines for anti-aliasing (smotther but a little slower)
anti-aliased lines don't take width

Draw single line
----------------

line draws a line from start to end co-ordinates.
Default width is 1. 

    import pygame
    from pygame.locals import *
    from sys import exit

    pygame.init()
    screen = pygame.display.set_mode((640, 480), 0, 32)

    while True:

        for event in pygame.event.get():
          if event.type == QUIT:
              pygame.quit()
              exit()
     
        screen.fill((255, 255, 255)) 

        mouse_pos = pygame.mouse.get_pos()

        for x in range(0,640,20):
            pygame.draw.line(screen, (0, 0, 0), (x, 0), mouse_pos)
            pygame.draw.line(screen, (0, 0, 0), (x, 479), mouse_pos)

        for y in range(0,480,20):
            pygame.draw.line(screen, (0, 0, 0), (0, y), mouse_pos)
            pygame.draw.line(screen, (0, 0, 0), (639, y), mouse_pos)

        pygame.display.update()
        
  

Draw multiple lines from co-odrinates
-------------------------------------

The following creates a 'snake' where points are added to from mouse position, but old points are
removed.

    import pygame
    from pygame.locals import *
    from sys import exit

    pygame.init()
    screen = pygame.display.set_mode((640, 480), 0, 32)

    points = []

    while True:

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                exit()
            if event.type == MOUSEMOTION: 
               points.append(event.pos) 
               if len(points)>100:
                   del points[0]

        screen.fill((255, 255, 255))

        if len(points)>1:
            pygame.draw.lines(screen, (0,255,0), False, points, 2)

        pygame.display.update()
