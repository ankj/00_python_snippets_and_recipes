#python
#numpy

# note: fancy indexing always copies the array into a new array

import numpy as np
import pprint
import random

pp = pprint.pprint  # make pp

arr = np.empty((8, 4))
for i in range(8):
    arr[i] = i
pp(arr)

# Select rows in a specified order

arr2 = arr[[4, 3, 0, 6]]
print()
pp(arr2)

# Use negative numbers to selwct from the end

arr2 = arr[[-1, -2]]
print()
pp(arr2)

import numpy as np
import pprint
import random

pp = pprint.pprint  # make pp

# __________________________________________________


## Reshaping arrays

arr = np.arange(32)
pp(arr)

arr2 = arr.reshape((8, 4))
print()
pp(arr2)

## creating a new matrix from a list of co-ordinates

arr3 = arr2[[1, 5, 7, 2], [0, 3, 1, 2]]  # prints 1,0; 5,3; 7,2; 2,2
print()
pp(arr3)

## Selecting rows and displaying columns in altered order

arr4 = arr2[[1, 5, 7, 2]][:, [0, 3, 1, 2]]
print()
pp(arr4)



