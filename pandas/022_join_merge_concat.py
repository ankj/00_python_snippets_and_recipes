#python
#pandas
#join
#merge
#concat


# concat finds common series names
# join joins based on index
# merge joins based on column values

# See also http://pandas.pydata.org/pandas-docs/stable/merging.html
# (diagrams in pandas manual)


import pandas as pd
import numpy as np

df1=pd.DataFrame({'name':['John','George','Ringo'],
                  'color':['Blue','Blue','Purple']})
df2=pd.DataFrame({'name':['Paul','George','Ringo'],
                  'carcolor':['Red','Blue',np.nan]},
                   index=[3,1,2])

# concatenate finds common series names
# use verify_integrity=True to error if duplicate index values created
print(pd.concat([df1,df2]))
print()
# To create new index....
print(pd.concat([df1,df2],ignore_index=True))
print()

# using axis=1 concat will add columns to matching index values
print(pd.concat([df1,df2],axis=1))
print()

# Join/Merge
# .join joins based on index
# .merge joins based on column values
# default for .merge is inner join (coumn names present in both data sets)
print(df1.merge(df2))
print()

# Outer, left and right join/merge
print(df1.merge(df2,how='outer'))
print()
print(df1.merge(df2,how='left'))
print()
print(df1.merge(df2,how='right'))
print()

# other merge parameters:
    # on (column names to join on)
    # left_on (column names for left data frame; used when names don't overlap)
    # right_on (as above but right column)
    # left_index (join based on left index, Boolean)
    # right_index (as above)
