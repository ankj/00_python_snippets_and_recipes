#python
#matplotlib
#bar chart

import numpy as np
import matplotlib.pyplot as plt


women_pop=np.array([50,30,45,22])
men_pop=np.array([5,25,50,20])
x=np.arange(4)

plt.barh(x,women_pop,color='0.25',edgecolor='0.0')
plt.barh(x,-men_pop,color='0.75',edgecolor='0.0')

plt.show()
