#python
#matplotlib
#3D
#scatter plot
#line plot

If run in iPython or run from command line (e.g. python temp.py) then mouse can be used to manipulate view.

import numpy as np
from mpl_toolkits.mplot3d import Axes3D # imports 3D extension
import matplotlib.pyplot as plot

#Lorenz dataset generation
a, b, c = 10., 28., 8. / 3.
def lorenz_map(X, dt = 1e-2):
    X_dt = numpy.array([a * (X[1] - X[0]),
                        X[0] * (b - X[2]) - X[1],
                        X[0] * X[1] - c * X[2]])
    return X + dt * X_dt

points = numpy.zeros((1000, 3))
X = numpy.array([.1, .0, .0])
for i in range(points.shape[0]):
    points[i], X = X, lorenz_map(X)

#Plot
fig = plot.figure() # create empty figure
ax = fig.gca(projection = '3d') # attach 3D axes to figure

ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')
ax.set_title('Lorenz Attractor a=%0.2f b=%0.2f c=%0.2f' %(a,b,c)) # ax rather plt is used

ax.scatter(points[:, 0], points[:, 1],  points[:, 2], c = 'k') # pass X,Y,Z to plot
plot.show()

# ====================================================================
# 3D scatter plot customisable in the same way as a normal plot, e.g.:
# ====================================================================

ax.scatter(points[:, 0], points[:, 1],  points[:, 2],
           marker='s',
           edgecolor='0.5',
           facecolor='0.5')Or line plot with:

with ax.plot(points[:, 0], points[:, 1],  points[:, 2], c = 'k')


