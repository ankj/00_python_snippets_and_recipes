#python
#numpy
#cast

import numpy as np
import pprint
pp=pprint.pprint # make pp shorthand for pprint.pprint


# Convert from list

data1 = [6, 7.5, 8, 0, 1]
arr1=np.array(data1)
pp(arr1)

data2 = [[ 1, 2, 3, 4], [5, 6, 7, 8]]
arr2 = np.array( data2)
pp(arr2)


print(arr2.ndim) # number of dimensions
print(arr2.shape) # array shape
print(arr1.dtype) # array data type


# filling an array at start

arr3=np.zeros((3,6))
print(arr3)

arr4=np.empty((2,3,2)) # note this often returns garbage values
print(arr4)

## Changing data type

print(arr2.dtype)
floatarray=arr2.astype(np.float64)
print(floatarray.dtype)
