﻿#python
#stats
#box plot
#boxplot

import numpy as np
import scipy.stats as stats
#import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

x=np.random.randn(100)
x=np.append(x,[5]) # add deliberate outlier

plt.boxplot(x,sym='*')
plt.show()
