#python
#debug


# pip install ipdb

import ipdb # to use iPython rather than python
print('hello world')
x=10
y=20
x=[1,2,3]

for i in range(3):
    print(i)
    
ipdb.set_trace() # enters iPython debug here
# pbd.trace # uses python command line rather than iPython 

for i in range (5):
    print(i)

ipdb.set_trace() # enters iPython debug here

"""Common commands when trace started:

n[ext]

n simply continues program execution to the next line in the current function. This will be your most-typed command.

s[tep]

s steps to the very next line of executable code, whether it’s inside a called method or just on the next line. I think of this as “step into” for tunneling into a function or method.

c[ontinue]

c continues program execution until another breakpoint is hit.

l[ist] [first[, last]]

l lists a larger portion of code–11 lines–moving down the file with successive calls. You can optionally provide a line number to center the view on, as well as a second number for a range.

p[rint] and pp

p and pp are print and pretty-print. pp uses the pprint module. I use pp almost exclusively.

a[rgs]

a is one of my favorites. It prints out all the arguments the current function received.

j[ump] line_number

j will jump to the given line of code, actually skipping the execution of anything between.

pp locals() and pp globals()

Use these commands to see all variables in the given scope, local or global. This is super useful for a quick glance at all the objects your program has access to at that moment.
