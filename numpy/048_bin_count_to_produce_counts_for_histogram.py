#python
#numpy
#bin count
#histogram

# note: use np.histpgram fo rmore versatility

import numpy as np

x=np.random.randint(1,10,100)
x=x*2
x_bin_count=np.bincount(x)
print(x_bin_count)

import matplotlib.pyplot as plt
x_labels=np.arange(0,len(x_bin_count))
plot_bins=plt.bar(x_labels,x_bin_count)
plt.xticks(x_labels)
plt.show()


