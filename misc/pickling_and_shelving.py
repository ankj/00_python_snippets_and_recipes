#python
#file
#pickle
#shelve


# Pickle It
# Demonstrates pickling and shelving data

import pickle, shelve

print("Pickling lists.")
variety = ["sweet", "hot", "dill"]
shape = ["whole", "spear", "chip"]
brand = ["Claussen", "Heinz", "Vlassic"]
f = open("pickles1.dat", "wb")
pickle.dump(variety, f)
pickle.dump(shape, f)
pickle.dump(brand, f)
f.close()

print("\nUnpickling lists.")
f = open("pickles1.dat", "rb")
variety = pickle.load(f)
shape = pickle.load(f)
brand = pickle.load(f)
print(variety)
print(shape)
print(brand)
f.close()

print("\nShelving lists.")
s = shelve.open("pickles2.dat")
s["variety"] = ["sweet", "hot", "dill"]
s["shape"] = ["whole", "spear", "chip"]
s["brand"] = ["Claussen", "Heinz", "Vlassic"]
s.sync()    # make sure data is written

print("\nRetrieving lists from a shelved file:")
print("brand -", s["brand"])
print("shape -", s["shape"])
print("variety -", s["variety"])
s.close()

input("\n\nPress the enter key to exit.")


# For access file modes (e.g. append, write +), see read and write to text files,
# but use rb, wb, ab+, etc. with 'b' indicating binary.
# Shelving allows random access to pickled objects - it is more powerful than picking and unpickling 
# for large data, though it does not have the complexity of databases.

-----------------------------------------------------------------------------

shelve example

# Shelve -a persitent data object that can hold any object type

import shelve
my_shelf = shelve.open('mydata.dat',writeback =True)
my_shelf['name']= ['Mohit', 'Ravi','Bhaskar'] # 'name' is the key
my_shelf['skill']= ['Python', 'Java','java']
my_shelf['age']= [27,25,25]
my_shelf.sync() # syncs shelf during program run (if writeback=True present)
my_shelf.close() # close and sync shelf (if writeback=True present)

#to retrieve data:
r = shelve.open('mydata.dat')
print('\nSaved data')
print (r['skill'])
print (r['age'])
print (r['name'])
r.close()

#to change data:
my_shelf2 = shelve.open('mydata.dat',writeback =True)
my_shelf2['skill'][1]='C'
my_shelf2.close()

# to check data is changed
print('\nchanged data data')
r2 = shelve.open('mydata.dat')
print (r2['skill'])
print (r2['age'])
print (r2['name'])
r2.close()

# append new data
my_shelf3 = shelve.open('mydata.dat',writeback =True)
my_shelf3['name'].append('Bob')
my_shelf3['skill'].append('Fortran')
my_shelf3['age'].append('65')
my_shelf3.close()

# to check data is appended
r3 = shelve.open('mydata.dat')
print('\nappended data')
print (r3['skill'])
print (r3['age'])
print (r3['name'])
r3.close()

Output:

Saved data
['Python', 'Java', 'java']
[27, 25, 25]
['Mohit', 'Ravi', 'Bhaskar']

changed data data
['Python', 'C', 'java']
[27, 25, 25]
['Mohit', 'Ravi', 'Bhaskar']

appended data
['Python', 'C', 'java', 'Fortran']
[27, 25, 25, '65']
['Mohit', 'Ravi', 'Bhaskar', 'Bob']

