#python
#oop
#class

# Construction (new class and instances of class)

_init_ method (the constructor) is called to create a new instance of class.

# All methods must have self as first parameter. E.g.

class cat:
  def __init__(self,colour,legs,name):
    self.name=name
    self.legs=legs
    self.colour=colour

felix=cat("Felix","ginger",4,name)
Print (felix.colour)

# output: ginger

# Methods

# In class statement...

  def bark(self):
    print("Woof! says"+self.name)

felix.bark()
# output: Woof! from Felix

# Class attributes

# Classes may have attributes (variables) assigned to the class (rather than instances), e.g.

class Dog:
  legs=4

# Use dir(Dog) to see all attributes
