#python
#machine learning
#sklearn
#matplotlib
#nearest neighbour



import numpy as np
import pandas as pd

# Original data load
#df_wine=pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data',header=None)
#df_wine.to_csv('wine.csv')
#remove generated index and header columns from csv manually

df_wine=pd.read_csv('wine.csv',header=None)
df_wine.columns=['Class label','Alcohol','Malic acid','Ash','Alcalinity of ash',
                 'Magnesium','Total phenols','Falvenoids','Non flavenoid phenols',
                 'Proanthpcyanins','Color intensity','Hue','OD280/OD315 of diluted wines',
                 'Proline']
print(np.unique(df_wine['Class label']))
print()

# Split the data set into training and test

from sklearn.cross_validation import train_test_split
X,y=df_wine.iloc[:,1:].values,df_wine.iloc[:,0].values # X=data, y=label
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.3,random_state=0)

# Feature scaling

# Normalisation
from sklearn.preprocessing import MinMaxScaler
mms=MinMaxScaler()
X_train_norm=mms.fit_transform(X_train)
X_test_norm=mms.fit_transform(X_test)

# Standardisation (mean=0, SD=1)
from sklearn.preprocessing import StandardScaler
stdsc=StandardScaler()
X_train_std=stdsc.fit_transform(X_train)
X_test_std=stdsc.fit_transform(X_test)

# Reducing complexity of fits with L1 regularisation
# L1 regularisation penalises total sum of weights (and less influential weights tend to be pushed to zero)

from sklearn.linear_model import LogisticRegression
LogisticRegression(penalty='l1')
# C is inverse of regularisation parameter
lr=LogisticRegression(penalty='l1',C=0.1)
lr.fit(X_train_std,y_train)
print('Training accuracy:',lr.score(X_train_std,y_train))
print()
print('Test accuracy:',lr.score(X_test_std,y_test))
print()
print('Model intercept terms',lr.intercept_)
print()
# The following shows model weights, one weight vector for each class
# Each class is based on separating itself from the other two classes
# The three weights vectors are used for each sample and a probability of it being each class calculated
# Class with highest probability is then selected
# Each vector has 13 weights, note sparcity - many are zero
print('Model weights',lr.coef_)
print()
# Print categorisation of first 5 test
print(lr.predict(X_test_std[0:5,:]))
print()
# Probabilities of first five tests
print(lr.predict_proba(X_test_std[0:5,:]))

"""Selecting features"""


from sklearn.base import clone
from itertools import combinations
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score

class SBS():
    def __init__(self, estimator, k_features, scoring=accuracy_score,
                 test_size=0.25, random_state=1):
        self.scoring = scoring
        self.estimator = clone(estimator)
        self.k_features = k_features
        self.test_size = test_size
        self.random_state = random_state

    def fit(self, X, y):

        X_train, X_test, y_train, y_test = \
                train_test_split(X, y, test_size=self.test_size,
                                 random_state=self.random_state)

        dim = X_train.shape[1]
        self.indices_ = tuple(range(dim))
        self.subsets_ = [self.indices_]
        score = self._calc_score(X_train, y_train,
                                 X_test, y_test, self.indices_)
        self.scores_ = [score]

        while dim > self.k_features:
            scores = []
            subsets = []

            for p in combinations(self.indices_, r=dim-1):
                score = self._calc_score(X_train, y_train,
                                         X_test, y_test, p)
                scores.append(score)
                subsets.append(p)

            best = np.argmax(scores)
            self.indices_ = subsets[best]
            self.subsets_.append(self.indices_)
            dim -= 1

            self.scores_.append(scores[best])
        self.k_score_ = self.scores_[-1]

        return self

    def transform(self, X):
        return X[:, self.indices_]

    def _calc_score(self, X_train, y_train, X_test, y_test, indices):
        self.estimator.fit(X_train[:, indices], y_train)
        y_pred = self.estimator.predict(X_test[:, indices])
        score = self.scoring(y_test, y_pred)
        return score

from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt

knn = KNeighborsClassifier(n_neighbors=2)

# selecting features
sbs = SBS(knn, k_features=1)
sbs.fit(X_train_std, y_train)

# plotting performance of feature subsets
k_feat = [len(k) for k in sbs.subsets_]

plt.plot(k_feat, sbs.scores_, marker='o')
plt.ylim([0.7, 1.1])
plt.ylabel('Accuracy')
plt.xlabel('Number of features')
plt.grid()
plt.tight_layout()
# plt.savefig('./sbs.png', dpi=300)
plt.show()
print(list(sbs.subsets_[8]))



Output plot:

[0, 1, 3, 10, 12
