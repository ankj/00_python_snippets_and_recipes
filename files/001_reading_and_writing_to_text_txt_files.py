#python
#files


# Read It
# Demonstrates reading from a text file

print("Opening and closing the file.")
text_file = open("read_it.txt", "r")
text_file.close()

print("\nReading characters from the file.")
text_file = open("read_it.txt", "r")
print(text_file.read(1))
print(text_file.read(5))
text_file.close()

print("\nReading the entire file at once.")
text_file = open("read_it.txt", "r")
whole_thing = text_file.read()
print(whole_thing)
text_file.close()

print("\nReading characters from a line.")
text_file = open("read_it.txt", "r")
print(text_file.readline(1))
print(text_file.readline(5))
text_file.close()

print("\nReading one line at a time.")
text_file = open("read_it.txt", "r")
print(text_file.readline())
print(text_file.readline())
print(text_file.readline())
text_file.close()

print("\nReading the entire file into a list.")
text_file = open("read_it.txt", "r")
lines = text_file.readlines()
print(lines)
print(len(lines))
for line in lines:
    print(line)
text_file.close()

print("\nLooping through the file, line by line.")
text_file = open("read_it.txt", "r")
for line in text_file:
    print(line)
text_file.close()

input("\n\nPress the enter key to exit.")


Text file access modes:

"r"     Read  (if file does not exist there will be an error)
"w"    Write If the file exists contents are overwritten. If the file does not exist, it is created.
"a"     Append - will add to file, or will create a file if no file exists
"r+"  Read from and write to a file
"w+"     Write to and read from a text file. If the file exists contents are overwritten. If the file does not exist, it is created.
"a+"     Append and read from a text file. If the file exists, new data is appended to it. If the files doesn't exist, it's created.

Writing to files:

# Write It
# Demonstrates writing to a text file

print("Creating a text file with the write() method.")
text_file = open("write_it.txt", "w")
text_file.write("Line 1\n")
text_file.write("This is line 2\n")
text_file.write("That makes this line 3\n")
text_file.close()

print("\nReading the newly created file.")
text_file = open("write_it.txt", "r")
print(text_file.read())
text_file.close()

print('\nCreating a text file with the writelines() method.')
text_file = open('write_it.txt', 'w')
lines = ['Line 1\n',
         'This is line 2\n',
         'That makes this line 3\n']
text_file.writelines(lines)
text_file.close()

print("\nReading the newly created file.")
text_file = open("write_it.txt", "r")
print(text_file.read())
text_file.close()

input("\n\nPress the enter key to exit.")


