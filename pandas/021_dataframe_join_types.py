#python
#pandas
#join

Dataset 1 (left) members: A B C
Dataset 2 (right) members: C D

Join types and resulting members:
=================================

Joins combine data from more than one table, but select members in different ways.

Inner: C but with data got C from both tables
Outer: A B C D with data from both tables
Left: A B C but data for A B C from both tables
Right: C D but data for C D from both tables

Inner: members must in both groups
Outer: members are in any group
