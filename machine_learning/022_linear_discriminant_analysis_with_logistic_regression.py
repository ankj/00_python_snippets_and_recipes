#python
#machine learning
#sklearn
#matplotlib


#%%  Import data

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#df_wine=pd.read_csv('wine.csv',header=None)
df_wine=pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data',header=None)

#%%  Split into training and test, and standardise
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import StandardScaler
X=df_wine.iloc[:,1:].values
y=df_wine.iloc[:,0].values
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.3,random_state=0)
sc=StandardScaler()
X_train_std=sc.fit_transform(X_train)
X_test_std=sc.fit_transform(X_test)

#%% Plot decision regions function

def plot_decision_regions(X,y,classifier,test_idx=None,resolution=0.02):
    from matplotlib.colors import ListedColormap
    # Set up marker generator and colour map
    markers=('s','x','o','^','v')
    colors=('red','blue','lightgreen','gray','cyan')
    cmap=ListedColormap(colors[:len(np.unique(y))])
    # Plot the decison surface
    x1_min,x1_max=X[:,0].min()-1,X[:,0].max()+1
    x2_min,x2_max=X[:,1].min()-1,X[:,1].max()+1
    xx1,xx2=np.meshgrid(np.arange(x1_min,x1_max,resolution),np.arange(x2_min,x2_max,resolution))
    Z=classifier.predict(np.array([xx1.ravel(),xx2.ravel()]).T)
    Z=Z.reshape(xx1.shape)
    plt.contourf(xx1,xx2,Z,alpha=0.4,cmap=cmap)
    plt.xlim(xx1.min(),xx1.max())
    plt.ylim(xx2.min(),xx2.max())
    # plot all samples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y==cl,0],y=X[y==cl,1],alpha=0.8,c=cmap(idx),marker=markers[idx],label=cl)
        # Highlight test samples
    if test_idx:
        X_test,y_test=X[test_idx,:],y[test_idx]
        plt.scatter(X_test[:,0],X_test[:,1],c='',alpha=1.0,linewidths=1,marker='o',s=55,label='test set')

#%% Set up LDA
from sklearn.lda import LDA
lda=LDA(n_components=2)
X_train_lda=lda.fit_transform(X_train_std,y_train)

#%% Apply logistic rogression
from sklearn.linear_model import LogisticRegression
lr=LogisticRegression()
lr=lr.fit(X_train_lda,y_train) # No regularisation
plot_decision_regions(X_train_lda,y_train,classifier=lr)
plt.xlabel('LD 1')
plt.ylabel('LD 2')
plt.legend(loc='lower left')
plt.show()

#%% Look at results of test set
X_test_LDA=lda.transform(X_test_std)
plot_decision_regions(X_train_lda,y_train,classifier=lr)
plt.xlabel('LD 1')
plt.ylabel('LD 2')
plt.legend(loc='lower left')
plt.show()

# Show probability of classification of five samples and show accuracy of classification
print(lr.predict_proba(X_test_LDA[0:5,:]))
y_pred=lr.predict(X_test_LDA)
from sklearn.metrics import accuracy_score
print('Accuracy: %.2f' %accuracy_score(y_test,y_pred))
