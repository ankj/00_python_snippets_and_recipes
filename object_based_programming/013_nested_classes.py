#python
#class
#oop

efficient use of classes! Each class only access the class under it: person, gun, magazine, bullet


01class bullet:
02    def fire(self):
03        print("Bang!")
04 
05class magazine:
06    def __init__(self):
07        self.max = 10
08        self.bullets = []
09    def fill(self.):
10        while len(self.bullets) < self.max:
11            self.bullets.append(bullet())
12    def fire(self):
13        if len(self.bullets) == 0:
14            print("Click")
15        else:
16            bullet = self.bullets.pop()
17            bullet.fire()
18 
19class gun:
20    def __init__(self):
21        self.magazine = magazine()
22    def load(self):
23        mag = magazine()
24        mag.fill()
25        self.magazine = mag
26    def fire(self):
27        self.magazine.fire()
28 
29class person:
30    def __init__(self):
31        self.gun = gun()
32    def fire(self, times = 1):
33        for i in range(times):
34            self.gun.fire()
35    def reload(self):
36        self.gun.load()
37 
38if __name__ == "__main__":
39    billy = person()
40    billy.reload()
41    billy.fire(3)
42    billy.reload()
43    billy.fire(11)
