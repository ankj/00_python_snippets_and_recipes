#python
#matplotlib
#axis

'You have several different questions here... Let me break them up a bit...
By "hide the axis labels on the first subplot" do you mean the actual axis labels (which aren't there unless you specify them),
the tick labels (i.e. the numbers along the axis), the axis ticks, or all of the above?

If you mean "all of the above", just do ax.xaxis.set_visible(False) and the same for the y-axis. 
(ax here would be vdsvgsPlot in your example code above)

If you mean the axis tick labels, just set them to [], i.e.: ax.set_xticklabels([]). (and set_yticklabels for the y-axis)

If you mean the axis ticks, you can do something similar: ax.set_xticks([]) and ax.set_yticks([]) which will turn off 
both the ticks and ticklabels.

As to the second question, use suptitle to title the entire figure. i.e.: 
fig.suptitle('whatever') (f.suptitle... in your example code above).

As for how to control the font properties, you can either pass various keyword arguments to 
suptitle (or anything else that creates text on a plot) or set them after you create the text. 
For example fig.suptitle('This is a title', size=20, horizontalalignment='left', font='Times', color='red')
