#python
#set
#union
#intersection
#difference


SetA={'Red','Blue','Green','Black'}
SetB={'Black','Green','Yellow','Orange'}
SetX=SetA.union(SetB)
SetY=SetA.intersection(SetB)
SetZ=SetA.difference(SetB)

In [6]: SetX
Out[6]: {'Black', 'Blue', 'Green', 'Orange', 'Red', 'Yellow'}

In [8]: SetY
Out[8]: {'Black', 'Green'}


In [9]: SetZ
Out[9]: {'Blue', 'Red'}

Testing for supersets and subsets

In [11]: SetA.issuperset(SetY)
Out[11]: True

In [12]: SetA.issubset(SetX)
Out[12]: True

Adding elements

In [13]: SetA.add('Purple')


In [14]: SetA
Out[14]: {'Black', 'Blue', 'Green', 'Purple', 'Red'}

Also:

For sets:
Sets may be combined:
| in either set
& must be in both sets
- in one set but not the other
^ in one but not both sets
