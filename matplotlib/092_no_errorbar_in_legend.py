#MAKE A CHART WITH ERROR BARS BETWEEN TWO LINES, BUT FOR THE LEGEND TO NOT INCLUDE THE ERROR BARS
#using pp=[] and "p=" (infront of the "ax1.plot" & "ax1.errorbar")
#Used https://stackoverflow.com/questions/14297714/matplotlib-dont-show-errorbars-in-legend

import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(12,5), dpi=300)

ax1 = fig.add_subplot(111)
pp = []

#dummy data
x=np.arange(0,6)
y=np.arange(2,8)
y2=np.arange(4,10)
y_up=y2-y
y_down=np.zeros(len(y_up))
error=[y_down,y_up]

#line 1
p=ax1.plot(x,y2,color='0.5',linestyle='dashed',label='worst case')
pp.append(p[0])

#line 2 with error bar
p=ax1.errorbar(x,y,
         color='k',
         yerr=error,
         errorevery=1,
         ecolor='0.5',
         label='best case')
pp.append(p[0])

#need this line to define "labels", used in legend(____)
handles, labels = ax1.get_legend_handles_labels()

ax1.legend(pp,labels,numpoints=1)

plt.savefig('no_errorbar_in_legend_chart.png',c='k',dpi=300)