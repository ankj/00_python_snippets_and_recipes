#python
#slice

Named slices can increase code readability

# Naming a slice
# Slice has start and end, end is NOT inclusive.
# Slices can also include a step

items=[0,1,2,4,8,16,32,64,128,256,512,1028,2056,4112,8224]
my_slice=slice(3,6)
print(items[my_slice])
# output:[4, 8, 16] (itemd 3-5 inclusive)

Named slices can also be used to change or delete values in objects.
Slice attributes can be accessed, e.g. for named slice s.....

s.start
s.stop
s.step

Slices can be mapped to, and automatically adjusted to fit, strings....

s='hello world'

a=slice(5,50,2)

a.indices(len(s))
Out[3]: (5, 11, 2)
