import numpy as np

def calculate_crowding(scores):
    # Crowding is based on a vector for each individual
    # All scores are normalised between low and high. For any one score, all
    # solutions are sorted in order low to high. Crowding for chromsome x
    # for that score is the difference between the next highest and next
    # lowest score. Total crowding value sums all crowding for all scores

    population_size = len(scores[:, 0])
    number_of_scores = len(scores[0, :])

    # create crowding matrix of population (row) and score (column)
    crowding_matrix = np.zeros((population_size, number_of_scores))

    # normalise scores (ptp is max-min)
    normed_scores = (scores - scores.min(0)) / scores.ptp(0)

    # calculate crowding distance for each score in turn
    for col in range(number_of_scores):
        crowding = np.zeros(population_size)

        # end points have maximum crowding
        crowding[0] = 1
        crowding[population_size - 1] = 1

        # Sort each score (to calculate crowding between adjacent scores)
        sorted_scores = np.sort(normed_scores[:, col])

        sorted_scores_index = np.argsort(
            normed_scores[:, col])

        # Calculate crowding distance for each individual
        crowding[1:population_size - 1] = \
            (sorted_scores[2:population_size] -
             sorted_scores[0:population_size - 2])

        # resort to orginal order (two steps)
        re_sort_order = np.argsort(sorted_scores_index)
        sorted_crowding = crowding[re_sort_order]

        # Record crowding distances
        crowding_matrix[:, col] = sorted_crowding

    # Sum croding distances of each score
    crowding_distances = np.sum(crowding_matrix, axis=1)

    return crowding_distances

test_array = np.array([[12, 0],
                       [11.5, 0.5],
                       [11, 1],
                       [10.8, 1.2],
                       [10.5, 1.5],
                       [10.3, 1.8],
                       [9.5, 2],
                       [9, 2.5],
                       [7, 3],
                       [5, 4],
                       [2.5, 6],
                       [2, 10],
                       [1.5, 11],
                       [1, 11.5],
                       [0.8, 11.7],
                       [0, 12]])

crowding_distance = calculate_crowding(test_array)

print(crowding_distance)

