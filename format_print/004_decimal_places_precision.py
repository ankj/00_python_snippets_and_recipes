import math
pi = math.pi
print ("Pi rounded to 3 decimal places is {0:.3f}".format(pi))
print ("Pi formated to 10 spaces is {0:10f}".format(pi))

# For Numpy decimal places
import numpy as np
np.set_printoptions(precision=2)
print(np.array([pi]))
# Default precision = 8