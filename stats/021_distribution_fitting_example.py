﻿#python
#stats

import pandas as pd
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.ticker as ticker
import statsmodels.api as sm

#data=pd.read_csv('los_for_distribution_analysis.csv')
combined = pd.concat([data['actual-model_l1'],
                     data['actual-model_l2'],
                     data['actual-model_l3'],
                     data['actual-model_l4']])
x = combined[~combined.isnull()]

mean = x.mean()
stdev = x.std()
median = x.median()
quartile1 = x.quantile(0.25)
quartile3 = x.quantile(0.75)
iqr = quartile3 - quartile1
low_cut_off = quartile1 -1.5*iqr
high_cut_off = quartile3 + 1.5*iqr

x_outlier_removed = x[x>low_cut_off]
x_outlier_removed = x_outlier_removed[x_outlier_removed<high_cut_off]
mean_outlier_removed =x_outlier_removed.mean()
stdev_outlier_removed = x_outlier_removed.std()

percent_outliers = 100-((x_outlier_removed.size / x.size) *100)
print(percent_outliers)


fig = plt.figure(figsize=(12,12),dpi=300)
fig.subplots_adjust(top=0.85)



## Plot distribution
ax1 = fig.add_subplot(221) # Grid of 1 x 32 this is suplot 1
#num_bins = 50
bins=np.arange(-5, 5.1, 0.2)
n, bins, patches = ax1.hist(x, bins=bins, normed=1,color='blue')
# add a 'best fit' line
y1 = mlab.normpdf(bins, mean, stdev)
ax1.plot(bins, y1, 'r--')
ax1.set_xlabel('ln(los) - ln(normalised los)')
ax1.set_ylabel('Probability density')
ax1.set_xlim(-4.2,4.2)
ax1.set_ylim(0,0.52)
ax1.xaxis.set_major_locator(ticker.MultipleLocator(1))
ax1.set_title('Distribution with no outlier removal',size=14)

ax2 = fig.add_subplot(222)
sm.graphics.qqplot(x, line='45',ax=ax2)
ax2.set_xlim(-4.2,4.2)
ax2.set_ylim(-4.2,4.2)
ax2.xaxis.set_major_locator(ticker.MultipleLocator(1))
ax2.yaxis.set_major_locator(ticker.MultipleLocator(1))
ax2.set_title('QQ plot with no outlier removal',size=14)

ax3 = fig.add_subplot(223) # Grid of 1 x 32 this is suplot 1
#num_bins = 50
bins=np.arange(-5, 5.1, 0.2)
n, bins, patches = ax3.hist(x_outlier_removed, bins=bins, normed=1,color='blue')
# add a 'best fit' line
y2 = mlab.normpdf(bins, mean_outlier_removed, stdev_outlier_removed)
ax3.plot(bins, y2, 'r--')
ax3.set_xlabel('ln(los) - ln(normalised los)')
ax3.set_ylabel('Probability density')
ax3.set_xlim(-3.2,3.2)
ax3.set_ylim(0,0.52)
ax3.xaxis.set_major_locator(ticker.MultipleLocator(1))
ax3.set_title('Distribution with outlier removal',size=14)


ax4 = fig.add_subplot(224)
sm.graphics.qqplot(x_outlier_removed, line='45',ax=ax4)
ax4.set_xlim(-3.2,3.2)
ax4.set_ylim(-3.2,3.2)
ax4.xaxis.set_major_locator(ticker.MultipleLocator(1))
ax4.yaxis.set_major_locator(ticker.MultipleLocator(1))
ax4.set_title('QQ plot with outlier removal',size=14)




plt.tight_layout(pad=3)
plt.savefig('summary_chart_1.tif',c='k',dpi=300,format='tif')
plt.show()