#python
#matplotlib
#bar chart

import matplotlib.pyplot as plt
import numpy as np

women_pop=np.array([50,30,45,22])
men_pop=np.array([5,25,50,20])
x=np.arange(4)

plt.barh(x,women_pop,color='r')
plt.barh(x,-men_pop,color='b')

plt.show()
