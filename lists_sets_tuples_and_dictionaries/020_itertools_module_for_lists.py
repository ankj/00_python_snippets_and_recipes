#python
#list

Itertools is a module with functions useful for lists. Functions include


	* Count (counts up from a value, e.g. For I in count(3) will count up from 3 until a break is used)
	* Cycle (infinitely iterates through an iterable)
	* Repeat (repeats either infinitely or a given number of times)
	* Takewhile (takes items from an iterable while a predicate function is true)
	* Chain (combines several iterables) 
	* Accumulate (returns a running total) 
	* Product (produces combinations of two,lists)
	* Permutations (produces permutations of elements of a list)


Need to import module or methods
