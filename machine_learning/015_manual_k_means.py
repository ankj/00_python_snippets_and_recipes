#python
#machine learning
#sklearn
#matplotlib
#k means

import numpy as np
import random
import matplotlib.pyplot as plt

NUMBER_OF_GROUPS=3
changeInGroupAllocation=True
iteration=0

def calculate_centroids():
    """ Function calculates centroid for each group in k_means
    Function accesses global variables:
        NUMBER_OF_GROUPS,numberOfAttributes,numberOfAttributes,iris_data
    """
    groupCentroids=np.zeros((NUMBER_OF_GROUPS,numberOfAttributes))
    for eachGroup in range(NUMBER_OF_GROUPS):
        irisDataForEachGroup=iris_data[currentAllocation==eachGroup,:]
        groupCentroids[eachGroup]=irisDataForEachGroup.mean(axis=0)
    return(groupCentroids)

def initiaite_clusters():
    # Initial assignment
    currentAllocation=np.random.randint(numberOfAttributes, size=numberOfItems)
    # Ensure each k is represented at least once by randomly assigning
    for i in range(NUMBER_OF_GROUPS):
        currentAllocation[random.randint(0,numberOfItems-1)]=i
    return currentAllocation

# Load data
from sklearn import datasets
iris=datasets.load_iris()
iris_data=iris.data
iris_type=iris.target
#iris_data=np.loadtxt('K_means_test.csv',delimiter=",")
numberOfItems=len(iris_data[:,0])
numberOfAttributes=len(iris_data[0,:])

# Initial assignment
currentAllocation=initiaite_clusters()

# First centroid calculation
centroids=calculate_centroids()
# assert False

# Iterate
while changeInGroupAllocation:
    iteration+=1
    print('Iteration: ',iteration)
    changeInGroupAllocation=False
    irisID=0
    for eachItem in iris_data:
       # Calculate Euclidian distance to each group centroid
       euclideanDistanceForEachGroup=np.zeros(NUMBER_OF_GROUPS)
       for group in range(NUMBER_OF_GROUPS):
           euclideanDistance=0
           for attributeID in range(numberOfAttributes):
               parameterDistance=(centroids[group,attributeID]-iris_data[irisID,attributeID])**2
               euclideanDistance+=parameterDistance                                     
           euclideanDistance=euclideanDistance**0.5
           euclideanDistanceForEachGroup[group]=euclideanDistance
       closestGroup=np.argmin(euclideanDistanceForEachGroup)
       # Check wheter item changes groups
       if currentAllocation[irisID]!=closestGroup:
           currentAllocation[irisID]=closestGroup
           changeInGroupAllocation=True
       irisID+=1
       centroids=calculate_centroids()
       # Simple method to cope with empty clusters: re-initiailise
       for centroid in centroids:
           if np.isnan(centroid.min()):
               currentAllocation=initiaite_clusters()

# plot
x=iris_data[:,0]
y=iris_data[:,1]
plt.scatter(x, y, s=50, c=currentAllocation,alpha=0.5)
plt.show()

 # Plot selecting symbol (this works but is slow)
x=iris_data[:,0]
y=iris_data[:,1]
markers = ['o', 'v', '*']
marker_color=['k','b','r']
for i in range(len(currentAllocation)):
    plt.scatter(x[i], y[i],s=100,marker=markers[currentAllocation[i]],color=marker_color[currentAllocation[i]])
plt.show()
 
 

                            
