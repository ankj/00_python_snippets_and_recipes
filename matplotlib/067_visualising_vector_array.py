#python
#matplotlib
#vector
#array

import numpy as np
import sympy
from sympy.abc import x,y
import matplotlib.pyplot as plt
import matplotlib.patches as patches

def cylinder_stream_function(U=1,R=1):
    r = sympy.sqrt(x**2 + y**2)
    theta = sympy.atan2(y,x)
    return U * (r-R ** 2/r) * sympy.sin(theta)

def velocty_field(psi):
    u=sympy.lambdify((x,y),psi.diff(y),'numpy')
    v=sympy.lambdify((x,y),-psi.diff(x),'numpy')
    return (u,v)

U_func,V_func = velocty_field(cylinder_stream_function())

xmin,xmax,ymin,ymax = -2.5,2.5,-2.5,2.5

Y,X=np.ogrid[ymin:ymax:16j,xmin:xmax:16j]
U,V=U_func(X,Y),V_func(X,Y)

# U & V store the vector field for co-ordinates X & Y

# The mask will hide vectors inside the circle
M=(X**2 + Y**2) <1
U=np.ma.masked_array(U,mask=M)
V=np.ma.masked_array(V,mask=M)

#Draw a circle in the middle:
shape = patches.Circle((0,0),radius=1,lw=2,fc='w',ec='k',zorder=0)

plt.gca().add_patch(shape)

# This is the vector plot. Pass X,Y,U&V to quiver
plt.quiver(X,Y,U,V,zorder=1)

plt.axes().set_aspect('equal')

plt.show()


