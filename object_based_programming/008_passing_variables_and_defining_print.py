#python
#oop
#print

Classes: passing variable and defining print info

class Pet (object):
    """A virtual pet"""
    number_of_legs=0

    def __init__(self,name): # this runs when a new instance is created
        number_of_legs=0
        print("A new pet has been born!")
        self.name=name
       
    def __str__(self): # this is what is retruned if a the object is printed
        rep="Critter object\n"
        rep+="Name: "+self.name+"\n"
        return rep

    def count_legs(self):
        print("I have %s legs.\n" % self.number_of_legs)

  

doug = Pet("Douggy")
doug.number_of_legs=3
print (doug)
doug.count_legs()
