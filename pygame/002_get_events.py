#python
#pygame

import pygame
from pygame.locals import *
from sys import exit

pygame.init()

SCREEN_SIZE = (800, 600)
screen = pygame.display.set_mode(SCREEN_SIZE, 0, 32)

while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            exit()

        print(event)  # prints event to the console (not the pygame screen)

        # Note: mouse event created when ever mouse moves. This is different to using
        # x, y = pygame.mouse.get_pos() which will get mouse position at a particular time

    pygame.display.update()

