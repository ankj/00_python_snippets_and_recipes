#python
#matplotlib
#3D
#wireframe
#surface

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

x=np.linspace(-3,3,256)
y=np.linspace(-3,3,256)
X,Y=np.meshgrid(x,y)
Z=np.sinc(np.sqrt(X**2 + Y**2))

fig=plt.figure()
ax=fig.gca(projection='3d')

# rstride: Array row stride (step size), defaults to 1
# cstride: Array column stride (step size), defaults to 1
# rcount: Use at most this many rows, defaults to 50
# ccount: Use at most this many columns, defaults to 50

ax.plot_wireframe(X,Y,Z,color='k',rcount=25,ccount=25)

plt.show()

