#import ipdb; ipdb.set_trace()

from sklearn import preprocessing

# encode labels to numbers with encoder fit
label_encoder = preprocessing.LabelEncoder()
input_classes = ['audi','ford','audi','toyota','ford','bmw']
label_encoder.fit(input_classes)

# show encoding
print ('\nClass mapping:')
for i,item in enumerate(label_encoder.classes_):
    print(item, '-->', i)

#transform another set of labels based on established encoding
labels = ['toyota','ford','audi']
encoded_labels = label_encoder.transform(labels)
print ('\nLabels =', labels)
print ('Encoded labels =', list(encoded_labels))

#transform label index number back to word labels
encoded_labels = [2,1,0,3,1]
decoded_labels = label_encoder.inverse_transform(encoded_labels)
print ('\nEncoded labels =', encoded_labels)
print ('Decoded labels =', decoded_labels)
