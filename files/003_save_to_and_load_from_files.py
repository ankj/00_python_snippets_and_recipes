#python
#file

fh = open("data.csv","w")
for i in range(10):
        print(i,file=fh,end=',')
print(file=fh) # creates new line (using \n will add another new line)
for i in range(10,20):
        print(i,file=fh,end=',')
fh.close()

with open("data.csv") as f:
    data = f.read() # reads one line at a time
    print(data)
