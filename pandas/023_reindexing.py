#python
#pandas
#index

In [7]: df.index.name
Out[7]: 'Index Title'

In [8]: df.index.name = 'foo'

In [9]: df.index.name
Out[9]: 'foo'

In [10]: df
Out[10]: 
         Column 1
foo              
Apples          1
Oranges         2
Puppies         3
Ducks           4

# ===========================================================================

import numpy as np
from pandas import Series,DataFrame
import pandas as pd
import pprint


pp=pprint.pprint # make pp


## Reindexing series
# Re-indexing re-arranges data and can add new index
obj=Series([4.5,7.2,-5.3,3.6],index=['d','b','a','c'])
print (obj,"\n")
obj2=obj.reindex(['a','b','c','d','e'])
print (obj2,"\n")
# Missing values can be filled
obj3=obj.reindex(['a','b','c','d','e'],fill_value=0)
print (obj3,"\n")


#ffil can add no values
#ffil carries values fowards, bfill carries values backwards from known index values
obj4=Series(['blue','purple','yellow'],index=[0,2,4])
obj5=obj4.reindex(range(6),method='ffill')
print (obj5,"\n")


##re-indexing data frames
frame = DataFrame(np.arange( 9). reshape((3, 3)), index =['a', 'c', 'd'],columns =['Ohio', 'Texas', 'California'])
print(frame,"\n")


#Re-indexing rows
frame2=frame.reindex(['a','b','c','d'])
print(frame2,"\n")


#Re-indexing columns
states = ['Texas', 'Utah', 'California']
frame3=frame.reindex(columns=states)
print(frame3,"\n")


#Re-indexing rowa nd columns (ffil only works row-wise)
frame4=frame.reindex(index =['a', 'b', 'c', 'd'], method ='ffill',columns = states)
print(frame4,"\n")


#Re-indexing may also be done by label-indexing with ix (no auto-fill):
frame5=frame.ix[['a','b','c','d'],states]
print(frame5,"\n")

