#python
#pygame

import pygame
from pygame.locals import *
from sys import exit

background_image_filename = 'sushiplate.jpg'
mouse_image_filename = 'fugu.png'

# Initialise pygame
# This command initialises all modules. Individual modules may be initialised,
# e.g. pygame.sound.innit()

pygame.init()

# Create display window - size, then flag (0 is no flag), then colour depth (bits)

# Flags:
#   FULLSCREEN
#   DOUBLEBUF   double buffered display recommended for HWSURFACE or OPENGL
#   HWSURFACE   hardware accelerated, must be combined with FULLSCREEN
#   OPENGL      creates an OpenGL rendered display
#   RESIZABLE   creates a resizable display
#   NOFRAME     removes border and title bar

# Colour depth (bits)
# 0 (or no value): screen colour depth
# 8: 256 colours
# 15: 32,768 colours (with spare bit)
# 16: 65,536 colours
# 24: 16.7 million colours
# 32: as 24 bit but with spare 8 bits

# This creates a 'surface' object stored in variable 'screen'

screen = pygame.display.set_mode((640, 480), 0, 32)

# Add caption
pygame.display.set_caption('Hello World!')

# Load background and create a surface; convert converts surface to same format as display
background = pygame.image.load(background_image_filename).convert()

# Load mouse. Alpha allows for transparency.
mouse_cursor = pygame.image.load(mouse_image_filename).convert_alpha()

while True:  # loop continuously

    # event action
    # events occur outside code (e.g. key press)
    # pygame.event.get() is list of events waiting (all events waiting when called)
    # pygame.event.wait() pauses all code and waits for event (not used here)
    # pygame.event.poll() retuens a sinlge event or type NOEVENT
    # pygame.event.pump() can be used in place of an event loop
    
    for event in pygame.event.get():
        # respond to the window close button
        if event.type == QUIT:
            pygame.quit()
            exit()

        # 'blit' copies the background to 'screen' Surface object. Tuple (0,0) is position
        # backgrounds needs to be continually refreshed to cover up cursor move

        screen.blit(background, (0, 0))

        # get current mouse position and unpack into x,y
        # without correction this would place top left of cursor image at mouse position
        # x and y moved by half width and height to centre image on mouse position
        x, y = pygame.mouse.get_pos()
        x -= mouse_cursor.get_width() / 2
        y -= mouse_cursor.get_height() / 2
        screen.blit(mouse_cursor, (x, y))

        # update display (it is built in a 'back buffer' above and copied in one go to avoid flicker
        pygame.display.update()

