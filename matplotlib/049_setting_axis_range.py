#python
#matplotlib
#axis

import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-6,6,1024)

plt.ylim(-5,1.5)
plt.plot(x,np.sinc(x),c='k')

plt.show()
