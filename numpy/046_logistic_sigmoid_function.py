#python
#numpy
#exponential
#logistic

expit is expoential function (converts x to y range 0-1, y=0.5 when x=0)
logit is inverse (x=0 when y=0.5)



scipy.special.expit
===================

The expit function, also known as the logistic function, is defined as expit(x) = 1/(1+exp(-x)). 
It is the inverse of the logit function.

Parameters:
x : ndarray
The ndarray to apply expit to element-wise.

Returns:
out : ndarray
An ndarray of the same shape as x. Its entries are expit of the corresponding entry of x.

scipy.special.logit
===================

The logit function is defined as logit(p) = log(p/(1-p)). Note that logit(0) = -inf, logit(1) = inf, and logit(p) for p<0 or p>1 yields nan.


Parameters:
x : ndarray
The ndarray to apply logit to element-wise.
Returns:
out : ndarray
An ndarray of the same shape as x. Its entries are logit of the corresponding entry of x.


