#python
#pandas
#append
#combine
#update

append: adds the new table below. Index is repeated (consider re-indexing)
combine: merges data using a function to choose or average data from two tables
update: update one table where matching index in another table


import pandas as pd

songs_66=pd.Series([3,10,11,15],
                   index=['George','Ringo','John','Paul'],
                   name='Counts')

songs_69=pd.Series([4,7,8,9],
                   index=['Ringo','George','John','Paul'],
                   name='Counts')

# Append
x=songs_66.append(songs_69)
print(x)
# This creates duplicate index values
# To force an error if duplicates ar emade use:
# x=songs_66.append(songs_69,verify_integrity=True) # will force an error

# Combine - uses a function on two values. Requires series to have the same indices
def avg(v1,v2):
    return (v1+v2)/2.0

x=songs_66.combine(songs_69, avg)
print('\n Combined series')
print(x)

# Update one series from another (matching fields will update)
print()
print('Original :')
print(songs_66)
songs_66.update(songs_69.iloc[0:2])
print('Updated :')
print(songs_66)

# .repeat(x) replicates each item x times
