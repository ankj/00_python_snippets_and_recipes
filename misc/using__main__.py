#python
#__main__

Using main()

Using main has the following uses:


	* Keeps all variables encapsulated inside main program; cannot be accessed from outside main() without reference
	* Allows main() to be at start of program, followed by other functions, with call to main at end
	* If used as a module protects the module from being run on loading


Used in this way:

Module called (module_storage.py).....

#%% These variables are accessable to program loading module
var1=10
var2='Hello world'

#%% This secion will not run when module is loaded but will run if run independently
def main():
    print('Hello from inside module main')

#%% This runs the main section but only if module is primary program
if __name__ == '__main__':
    main()

Main program calling module:

def main():
    import module_storage
    print('Imported from module: ',module_storage.var1)
    print('Imported from module: ',module_storage.var2)
    print('Variables in called module: ',dir(module_storage))
    # module_storage.main() would run imported module main function

if __name__ == '__main__':
    main()
