#python
#matplotlib
#axis
#log


import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-100,100,4096)


plt.xscale('symlog',linthresh=6) # uses linear scale for -6 to 6
plt.plot(x,np.sinc(x),c='k')

plt.show()
