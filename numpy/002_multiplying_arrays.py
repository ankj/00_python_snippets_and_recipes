#python
#numpy

import numpy as np
import pprint
pp=pprint.pprint # make pp


## Set up arrays
array1=np.array([1,2,3,4,5])
data2=([1],[2],[3],[4],[5])
array2=np.array(data2)


## Multiply arrays of different shapes
array3=array1*array2
pp(array3)


## Multiply arrays of same shape
array4=array3 * array3
pp (array4)

## Multiply array by scaler
array4=array4*3
pp (array4)


