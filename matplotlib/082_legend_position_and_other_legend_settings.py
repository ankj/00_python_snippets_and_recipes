#python
#matplotlib
#legend

Legend position:
plt.legend(loc=('upper left') # upper, center, lower and left,centre,right
plt.legend(loc=(x,y) # where x,y of 0-1 are in plot area and other numbers are outside plot area

To show just a marker and no lines
plt.legend(numpoints=1)
plt.legend(scatterpoints=1) # to adjust scatter plot legend

Other methods:
plt.legend(framealpha=0.5) # set transparency between 0 and 1
plt.legend(ncols=2) #  number of columns
plt.legend(title) # title
