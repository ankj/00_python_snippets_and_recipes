#python
#matplotlib

import matplotlib.patches as patches
import matplotlib.pyplot as plt

# Circle
shape=patches.Circle((0,0),radius=1,color='0.75')
plt.gca().add_patch(shape)

# Rectangle
shape=patches.Rectangle((2.5,-5),2,1,color='0.75')
plt.gca().add_patch(shape)

# Fancy box
shape=patches.FancyBboxPatch((2.5,-2.5),2,1,boxstyle='sawtooth',color='0.75')
plt.gca().add_patch(shape)

# Ellipse
shape=patches.Ellipse((0,-2),2,1,angle=45,color='0.75')
plt.gca().add_patch(shape)

#Display all
plt.grid(True)
plt.axis('scaled')
plt.show()

# ==========================================================================
# Polygons may also be plotted by giving MatPlotLib a table of co-ordinates:
# ==========================================================================

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

theta=np.linspace(0,2*np.pi,8)
points=np.vstack((np.cos(theta),np.sin(theta))).T

plt.gca().add_patch(patches.Polygon(points,color='.75'))

plt.grid(True)
plt.axis('scaled')
plt.show()              


"""The points generated for polygon above are:

1.000000000000000000e+00    0.000000000000000000e+00
6.234898018587335944e-01    7.818314824680298036e-01
-2.225209339563143374e-01    9.749279121818236193e-01
-9.009688679024190350e-01    4.338837391175582314e-01
-9.009688679024191460e-01    -4.338837391175580094e-01
-2.225209339563145872e-01    -9.749279121818236193e-01
6.234898018587333723e-01    -7.818314824680299147e-01
1.000000000000000000e+00    -2.449293598294706414e-16

Further formatting is possible, e.g.:

plt.gca().add_patch(patches.Polygon(points,
       closed=None,
       fill=True,
       facecolor='y',
       lw=3,
       ls='dashed',
       edgecolor='k'))
"""