#python
#matplotlib
#grid

import matplotlib.pyplot as plt
import numpy as np

x=np.linspace(0,6,1024)
y1=np.sin(x)
y2=np.cos(x)

plt.xlabel('x')
plt.ylabel('y')
plt.plot(x,y1,c='k',lw=3,label='sin(x)')
plt.plot(x,y2,c='r',lw=3,label='cos(x)')
plt.legend(loc='lower left',title='Function',ncol=1)
plt.grid(True,lw=1,ls='--',c='.75')

plt.show()
