#python
#numpy
#array_split


numpy.array_split(ary, indices_or_sections, axis=0)

Split an array into multiple sub-arrays.

Please refer to the split documentation. 
The only difference between these functions is that array_split allows indices_or_sections to be 
an integer that does not equally divide the axis.

Examples
>>>
>>> x = np.arange(8.0)
>>> np.array_split(x, 3)
    [array([ 0.,  1.,  2.]), array([ 3.,  4.,  5.]), array([ 6.,  7.])]