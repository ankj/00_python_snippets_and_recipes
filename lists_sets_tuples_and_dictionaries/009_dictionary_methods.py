def print_bird_dict():
    for bird, count in birds_observed.items():
        print (bird, count)


observations=['canada goose',
       'canada goose',
       'long-tailed jaeger',
       'canada goose ',
       'snow goose',
       'canada goose',
       ' long-tailed jaeger',
       'canada goose',
       'northern fulmar']

# Create dictionaryt to count birds observed
birds_observed = {}

# Loop through observation list (this could be an open file)
for line in observations:
    bird = line.strip()
    if bird in birds_observed:
        # bird is the key in the dictionary
        birds_observed[bird] += 1
    else:
        birds_observed[bird] = 1

# Loop through and print results using item
for bird, count in birds_observed.items():
    print (bird, count)
    
# Print results
print_bird_dict

# Dictionary methods
# ==================

x = 'canada goose'
print('\nPrint an item:')
print (x,birds_observed[x])

# Usual method of getting a value will return an error item does not exist
# Can use if/try or use .get method which return null if not existing

print('\nPrint an item using .get:')
x = 'canada goose'
print (x,birds_observed.get(x))

print('\nPrint a non-existing item using .get:')
x = 'canada grouse'
print (x,birds_observed.get(x))

# Or return a default value
print('\nPrint a default value for a non-existing item using .get:')
x = 'canada grouse'
print (x,birds_observed.get(x, 'Not seen'))

# Return dictionary as list of tuples
print('\nPrint a list of the dictionary:')
print (list(birds_observed.items()))

# Return dictionary keys as list
print('\nPrint a list of keys:')
print (list(birds_observed.keys()))

print('\nPrint a list of values')
# Return dictionary values as list
print (list(birds_observed.values()))

# Pop an item
print('\nPop northern fulmar:')
print(birds_observed.pop('northern fulmar'))
print('\nRemaining list after pop:')
print_bird_dict()

# Append or change item from other dictionary
print('\nUpdate items fron other dictionary:')
new_obs = {'canada goose':10,'sparrow':16}
birds_observed.update(new_obs)
print_bird_dict()

# Clear all dictionary
#birds_observed.clear() # deletes all items
#print ('\nClear dictionary with .clear:')
#print_bird_dict()