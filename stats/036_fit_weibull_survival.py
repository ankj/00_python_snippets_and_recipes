"""
Weibull survival distribution
"""

import matplotlib.pyplot as plt
import scipy as sp
from scipy import stats

# Generate example data with a Weibull modulus of 1.5
WeibulDist = stats.weibull_min(1.5)
data = WeibulDist.rvs(25000)

# Now fit
fitPars = stats.weibull_min.fit(data)


# Output results
plt.hist(data,bins=50)
print('The fitted Weibul modulus is {0:5.2f}, compared to the exact calue of 1.5.'.format(fitPars[0]))


