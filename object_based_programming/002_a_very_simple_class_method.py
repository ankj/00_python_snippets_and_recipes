#python
#oop
#class


class Point:
    def reset(self):
        self.x=0
        self.y=0

    def move(self,x,y):
        self.x=x
        self.y=y

p=Point()
p.x=100
p.y=50
print(p.x, p.y)
p.reset() # Point.reset(p) would do the same
print(p.x, p.y)
p.move(20,30)
print(p.x, p.y)
