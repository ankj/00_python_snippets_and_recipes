#python
#pygame

# Create display window - size, then flag (0 is no flag), then colour depth (bits)

# Flags:
#   FULLSCREEN
#   DOUBLEBUF   double buffered display recommended for HWSURFACE or OPENGL
#   HWSURFACE   hardware accelerated, must be combined with FULLSCREEN
#   OPENGL      creates a 3D OpenGL rendered display
#   RESIZABLE   creates a resizable display
#   NOFRAME     removes border and title bar


Combine with following syntax:

screen = pygame.display.set_mode(SCREEN_SIZE, DOUBLEBUF | HWSURFACE | FULLSCREEN, 32)

Generally use no flag (0) or full screen.

HWSURFACE uses graphics card memory but may not work so well on non-Windows computers.
DOUBLEBUF prpeares a buffered screen ready to copy directly to screen, so can run faster.

When using DOUBLEBUF use pygame.display.flip() rather than pygame.display.update()


