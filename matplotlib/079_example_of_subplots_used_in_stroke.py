#python
#matplotlib
#scatter
#subplot
#axis

# *** Data for these plots does not exist here ***

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker

    # Score_matrix:
    # 0: Number of hospitals
    # 1: Average distance
    # 2: Maximum distance
    # 3: Maximum admissions to any one hopsital
    # 4: Minimum admissions to any one hopsital
    # 5: Max/Min Admissions ratio
    # 6: Proportion patients within target distance 1
    # 7: Proportion patients within target distance 2
    # 8: Proportion patients within target distance 3
    # 9: Proportion patients attending unit with target admission numbers
    # 10: Proportion of patients meeting distance 1 (~30 min) and admissions target
    # 11: Proportion of patients meeting distance 2 (~45 min) and admissions target
    # 12: Proportion of patients meeting distance 3 (~60 min) and admissions target
    # 13: Clinical benefit, additional benefit per 100 treatable patients

load_new_data=True

if load_new_data:
    data=np.loadtxt('scores_hospitals.csv',delimiter=",")
    data=data[:,0:14]
    cols=['hospitals',
          'av_time',
          'max_time',
          'max_admissions',
          'min_admissions',
          'max_min_admissions_ratio',
          'prop_target_time_1',
          'prop_target_time_2',
          'prop_target_time_3',
          'prop_target_admissions',
          'prop_target_admissions_time_1',
          'prop_target_admissions_time_2',
          'prop_target_admissions_time_3',
          'clinical_benefit']

    data_df=pd.DataFrame(data,columns=cols)
    data_df.to_csv('Scenario_DataFrame')

max_units=data_df['hospitals'].max()

fig = plt.figure(figsize=(12.5,5.5), dpi=300)

#%% Target admissions

ax1 = fig.add_subplot(131) # Grid of 1 x 3, this is suplot 1

x=data_df['hospitals']
y=data_df['prop_target_admissions']*100

ax1.scatter(x,y,
            color='w',
            edgecolor='k',
            marker='o',
            s=32,
            label='')

ax1.xaxis.set_major_locator(ticker.MultipleLocator(20))
ax1.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax1.yaxis.set_major_locator(ticker.MultipleLocator(20))
ax1.yaxis.set_minor_locator(ticker.MultipleLocator(10))
ax1.set_xlim(-5,max_units+5) # keeps axes same on all subplots
ax1.set_ylim(-5,105)
ax1.grid(True, which='both') # which may be 'major', 'minor' or 'both'
ax1.set_xlabel('Number of units')
ax1.set_ylabel('Percent patients attending unit with target admissions')


#%% Target distance

ax2 = fig.add_subplot(1,3,2)

x=data_df['hospitals']
y=data_df['prop_target_time_2']*100

ax2.scatter(x,y,
            color='w',
            edgecolor='k',
            marker='s',
            s=32,
            label='')

ax2.xaxis.set_major_locator(ticker.MultipleLocator(20))
ax2.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax2.yaxis.set_major_locator(ticker.MultipleLocator(20))
ax2.yaxis.set_minor_locator(ticker.MultipleLocator(10))
ax2.set_xlim(-5,max_units+5)
ax2.set_ylim(-5,105)
ax2.grid(True, which='both') # which may be 'major', 'minor' or 'both'
ax2.set_xlabel('Number of units')
ax2.set_ylabel('Percent patients meeting target travel time')

#%% Target distance and admissions

fig.add_subplot(1,3,1) # Grid of 1 x 3, this is suplot 1

ax3 = fig.add_subplot(1,3,3)

x=data_df['hospitals']
y=data_df['prop_target_admissions_time_2']*100

ax3.scatter(x,y,
            color='w',
            edgecolor='k',
            marker='^',
            s=32,
            label='')

ax3.xaxis.set_major_locator(ticker.MultipleLocator(20))
ax3.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax3.yaxis.set_major_locator(ticker.MultipleLocator(20))
ax3.yaxis.set_minor_locator(ticker.MultipleLocator(10))
ax3.set_xlim(-5,max_units+5)
ax3.set_ylim(-5,105)
ax3.grid(True, which='both') # which may be 'major', 'minor' or 'both'
ax3.set_xlabel('Number of units')
ax3.set_ylabel('Percent patients meeting target travel time and admissions')

plt.suptitle('Achieving dual target of 1) untits with 1000 admissions/year, and 2) all admissions within 45 minutes')

plt.tight_layout(pad=3)
plt.savefig('summary_chart.png',c='k',dpi=300)
plt.show()
