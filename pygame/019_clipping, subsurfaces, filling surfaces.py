#python
#pygame


Clipping
--------

Cliiping allows for just part of the screen to be changed and updated.

screen.set_clip(x,y,width,height)

.get_clip returns the set clipping area


Subsurfaces
-----------

Subsurfaces exist inside another surface. When the sub-surface is updated then the parent surface is
also updated. May be used, for example, for fancy fonts stires as images.

my_subsurface = my_image.suburface((x,y),(widhth,height))

e.g. could store letter subsurface in a dictionary.



Filling surfaces
----------------

Commonly used to clear screen as part of animation

screen.fill((0,0,0)) # fills with black

the .fill method also takes an optional rectangle argument which can be a convenient way to draw a
solid rectangle.



