﻿#python
#statistics
#scipy
#statsmodels



import numpy as np
import matplotlib.pyplot as plt

x=np.array([1,2,3,4,5,6,7,8,9,10])
y=np.array([2.3,4.5,5.0,8,11.1,10.9,13.9,15.4,18.2,19.5])
y=y+10

# scipy linear regression
from scipy import stats
gradient, intercept, r_value, p_value, std_err = stats.linregress(x,y)
y_fit=intercept+x*gradient

plt.plot(x, y, 'o', label='original data')
plt.plot(x, y_fit, 'r', label='fitted line')


text='Intercept: %.1f\nslope: %.2f\nR-square: %.3f' %(intercept,gradient,r_value**2)

plt.text(6,15,text)
plt.legend()
plt.show()

# stats models regression
import statsmodels.api as sm

x_for_fit = sm.add_constant(x) # Leave this out for fitting without intercept
model = sm.OLS(y,x_for_fit)
print(model.summary())
results = model.fit()
intercept=results.params[0]
gradient=results.params[1]
r_square = results.rsquared
y_fit=intercept+x*gradient


plt.plot(x, y, 'o', label='original data')
plt.plot(x, y_fit, 'r', label='fitted line')

text='Intercept: %.1f\nslope: %.2f\nR-square: %.3f' %(intercept,gradient,r_square)

plt.text(6,15,text)
plt.legend()
plt.show()

print(results.summary())
