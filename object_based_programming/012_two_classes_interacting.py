#python
#oop

class Parent(object):
    """The parent"""
    def order_bed (self,child): # see comment below
        print("Go to bed child!!!")
        child.go_to_bed() # note this is child as defined in definition above

class Child(object):
    def go_to_bed(self):
        print("Awwww. OK. Goodnight")

# Main

dad=Parent()
kid=Child()
dad.order_bed(kid)