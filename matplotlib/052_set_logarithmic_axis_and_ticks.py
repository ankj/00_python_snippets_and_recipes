#python
#matplotlib
#log
#axis
#tick

# ===============
# using log scale
# ===============

import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(1,10,1024)

plt.yscale('log')

plt.plot(x,x,c='k',lw=2,label=r'$f(x)=x$')
plt.plot(x,10**x,c='.75',lw=2,label=r'$f(x)=10^x$')
plt.plot(x,np.log(x),c='.75',lw=2,label=r'$f(x)=\log(x)$')

plt.legend()
plt.show()


# ==========================================================
# adjusting tick marks (force mark every order of magnitude)
# ==========================================================

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

x=np.linspace(1,10,1024)

plt.yscale('log')
ax=plt.axes()

yticks=[1e-2,.1,1,10,1e2,1e3,1e4,1e5,1e6,1e7,1e8,1e9]

ax.yaxis.set_major_locator(ticker.FixedLocator(yticks))

subsy=[2, 3, 4, 5, 6, 7, 8, 9]

plt.plot(x,x,c='k',lw=2,label=r'$f(x)=x$')
plt.plot(x,10**x,c='.75',lw=2,label=r'$f(x)=10^x$')
plt.plot(x,np.log(x),c='.75',lw=2,label=r'$f(x)=\log(x)$')

plt.legend()
plt.show()
